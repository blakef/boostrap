# All the hard parts

This contains all of the painful stuff to get going:

- webpack
- babel
- typescript

To use:

```bash
npm i
npm run build
npm run server
```
