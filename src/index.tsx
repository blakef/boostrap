import CodeMirror, {Editor} from "codemirror";
import "codemirror/mode/javascript/javascript";
import ReactDOM from "react-dom";
import React, {useEffect, useRef, useState} from "react";

interface ExampleProps {
    header: string;
    children: string;
}

// See: https://www.typescriptlang.org/docs/handbook/declaration-files/by-example.html, which webpack injects
declare var SOURCE_FILE: string;

const Example: React.FC<ExampleProps> = ({ header, children }) => {
    const ref = useRef<HTMLDivElement>(null);
    const [mirror, setMirror] = useState<Editor | null>(null);

    useEffect(() => {
        if (!ref.current) {
            return;
        }
        const code = CodeMirror(ref.current, {
            value: SOURCE_FILE,
            mode: "javascript"
        });
        setMirror(code);
        return () => {
            // TODO: add clean up code to remove CodeMirror
        };
    }, []);

    return (
        <div className="funk">
            <h1>{header}</h1>
            <div ref={ref} />
            <footer>{children}</footer>
        </div>
    );
};

ReactDOM.render(
    <Example header="Oh yea!">Here is some text</Example>, document.getElementById('app')
);
