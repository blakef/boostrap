const path = require('path');
const webpack = require('webpack');

const entry = './src/index.tsx';
const meta = require('fs').readFileSync(entry).toString();

module.exports = {
    mode: 'development',
    entry: './src/index.tsx',
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'index.js'
    },
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                exclude: /(node_modules)/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env']
                    },
                }
            }
        ]
    },
    plugins: [
        new webpack.DefinePlugin({
            SOURCE_FILE: JSON.stringify(meta),
        })
    ]
};
